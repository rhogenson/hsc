{-# LANGUAGE OverloadedStrings #-}

{-
 - Copyright 2018 Rose Hogenson
 -
 - This file is part of HSC.

 - HSC is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 - 
 - HSC is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 - 
 - You should have received a copy of the GNU General Public License
 - along with HSC.  If not, see <http://www.gnu.org/licenses/>.
 -}

import Graphics.UI.Gtk
import qualified Control.Monad.IO.Class as Cl
import qualified RealCalc

main :: IO ()
main = do _ <- initGUI
          w <- windowNew
          set w [ windowDecorated := False, windowResizable := False ]
          grid <- gridNew
          input <- entryNew
          output <- entryNew
          set output [ entryEditable := False ]
          clip <- Cl.liftIO $ clipboardGet selectionPrimary
          gridAttach grid input 0 0 1 1
          gridAttach grid output 0 1 1 1
          containerAdd w grid
          _ <- w `on` deleteEvent $ do
            Cl.liftIO mainQuit
            return True
          _ <- input `on` keyReleaseEvent $ do
            k <- eventKeyName
            let quit = Cl.liftIO mainQuit
            case k of
              "Escape" -> quit
              "Return" -> quit
              "bracketleft" -> do
                m <- eventModifier
                if Control `elem` m
                  then quit
                  else return ()
              _ -> return ()
            s <- Cl.liftIO $ entryGetText input
            case RealCalc.calculate s of
              Nothing -> return ()
              Just r -> do Cl.liftIO $ clipboardSetText clip r
                           Cl.liftIO $ set output [ entryText := r ]
            return True
          widgetShowAll w
          mainGUI
